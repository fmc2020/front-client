import Vue from 'vue';
import app from './app/index';

global.Vue = Vue;

Vue.config.productionTip = false;

App.init();
