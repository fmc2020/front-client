import Mobile from '../components/mobile/App.vue';
import Desktop from '../components/desktop/App.vue';
import router from '../router';
import * as axios from 'axios';
import store from '../store';
import vuetify from '../plugins/vuetify'

global.router = router;
global.axios = axios;
global.url = (u) => {
	return `http://192.168.1.64:8888/${u}`
};
global.Hammer = require('hammerjs');

function getReqConfig() {
	let headers = {};
	let session = localStorage.getItem('session');
	if (session) session = JSON.parse(session);
	if (session && session.token) {
		headers.user_token = session.token;
	}
	return {headers}
}

global.post = (url, data) => {
	return new Promise((res, rej) => {
		axios.post(url, data, getReqConfig()).then(result => {
			result = postData(result);
			if (result.type === 'err') rej(result.msg);
			if (result.type === 'su') res(result.data);
		})
	});
}

global.get = (url) => {
	return new Promise((res, rej) => {
		axios.get(url, getReqConfig()).then(result => {
			result = postData(result);
			if (result.type === 'err') rej(result.msg);
			if (result.type === 'su') res(result.data);
		})
	});
}

global.postData = (data) => {
	return data.data;
};

global.App = new class {

	constructor() {
		this.isMobile = true;
		this.User = require('./user');
	}


	init() {
		window.addEventListener('resize', this.mountPage.bind(this));
		this.initMain();
	}

	initMain() {
		this.routeListener();
		this.mountPage();
		this.auth();
	}

	auth(path = '/') {
		
		if (['/auth', '/about'].includes(path))
			path = '';
		
		let user = localStorage.getItem('user');
		if (user) user = JSON.parse(user);
		
		if (!user) {
          return '/auth';
		}
		
		if (!user.name || !user.tags || !user.tags.length) {
			return '/about';
		}
		
		if (!path)
			return '/'
	}

	routeListener() {
		router.beforeEach((to, from, nextRoute) => {
			let path = this.auth(to.path);
			if (path !== to.path)
				router.push(path);
			nextRoute();
		});
	}

	mountPage() {
		this.checkScreenWidth();

		if (this.vuePage && this.isMobile)
			return;

		if (this.vuePage)
			this.vuePage.$destroy();

		this.vuePage = new Vue({
			render: h => (this.isMobile ? h(Mobile) : h(Desktop)),
			vuetify,
			router,
			store,
		}).$mount('#app');
	}

	checkScreenWidth() {
		this.isMobile = window.innerWidth < 1000;
	}
};
