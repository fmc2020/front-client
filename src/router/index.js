import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		component: () => import('../components/mobile/dialogs/DialogsList.vue'),
	},
	{
		path: '/auth',
		name: 'auth',
		component: () => import('../components/mobile/auth/Auth.vue'),
	},
	{
		path: '/reg',
		name: 'reg',
		component: () => import('../components/mobile/auth/Reg.vue'),
	},
	{
		path: '/about',
		name: 'about',
		component: () => import('../components/mobile/about/About.vue'),
	},
];

const router = new VueRouter({
	mode: 'history',
	routes,
});

export default router;
